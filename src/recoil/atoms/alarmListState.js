import { atom } from 'recoil';

// default will the the data structure you want to subscribe to
export const alarmListState = atom({
    key: 'alarmListState',
    default: [],
});
