import {
    selector,
} from 'recoil';
import {charState} from "../atoms/charState";

export const charCountSelector = selector({
    key: 'Alarmcountstate', // unique ID (with respect to other atoms/selectors)
    get: ({get}) => {
        const alarm = get(charState);

        return alarm.length;
    },
});
