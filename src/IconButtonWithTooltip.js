import React from 'react';
import { Box, IconButton, Tooltip } from '@chakra-ui/react';

export default function IconButtonWithTooltip({
                                                  label,
                                                  placement,
                                                  disabled,
                                                  tooltipProp,
                                                  ariaLabel,
                                                  colorScheme,
                                                  color,
                                                  icon,
                                                  mr ,
                                                  onClick,
                                                  p,
                                                  rest ,
                                                  size ,
                                                  style ,
                                                  variant ,
                                                  loading,
                                              }) {
    if (color){
        return (
            <Tooltip label={label} placement={placement} tooltipProp={tooltipProp}>
                <Box cursor={disabled ? 'not-allowed' : 'inherit'}>
                    <IconButton
                        disabled={disabled}
                        pointerEvents={disabled ? 'none' : 'inherit'}
                        icon={icon}
                        aria-label={ariaLabel}
                        colorScheme={colorScheme}
                        color={color}
                        mr={mr}
                        onClick={onClick}
                        p={p}
                        rest={rest}
                        size={size}
                        style={style}
                        variant={variant}
                        isLoading={loading}
                    />
                </Box>
            </Tooltip>
        )
    }
    return (
        <Tooltip label={label} placement={placement} tooltipProp={tooltipProp}>
            <Box d='inline' cursor={disabled ? 'not-allowed' : 'inherit'}>
                <IconButton
                    disabled={disabled}
                    pointerEvents={disabled ? 'none' : 'inherit'}
                    icon={icon}
                    aria-label={ariaLabel}
                    mr={mr}
                    onClick={onClick}
                    p={p}
                    rest={rest}
                    size={size}
                    style={style}
                    variant={variant}
                    isLoading={loading}
                />
            </Box>
        </Tooltip>
    );
}
