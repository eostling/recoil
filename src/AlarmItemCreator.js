import { useSetRecoilState} from 'recoil';
import {useState} from "react";
import {alarmListState} from "./recoil/atoms/alarmListState";

const globalAlarms = [
    {
        "label": {
            "sensorName": "1904A_K9_ARMAG_(SFS)_Keypad",
            "alarmType": "TAMPER"
        },
        "sensorName": "1904A_K9_ARMAG_(SFS)_Keypad",
        "alarmType": "TAMPER",
        "sensorType": "LINEAR2000E",
        "lat": 35.3580833,
        "lon": -77.9698056,
        "geohash": "dq291u8pg",
        "zone": 5,
        "facilityID": "1904",
        "floor": 1,
        "assetNumber": "TBD",
        "lastModified": 1633440297463,
        "notes": "",
        "alarmID": "f913c7c9-85a9-4ec5-9bf9-f097e91a96bd",
        "priority": 1,
        "category": "SECURITY",
        "groupAccess": false,
        "operatorAccess": false,
        "active": false,
        "acknowledgementTimeout": false,
        "facilityName": "Vet Clinic",
        "stateInputs": {
            "sensorReported": "SECURE",
            "workflowRequested": "SECURE",
            "userRequested": "SECURE"
        },
        "state": "SECURE",
        "alarmTime": -1,
        "additionalAlarmDetails": {},
        "nextValidActions": {
            "access": {
                "displayProperties": {
                    "isDisabled": true,
                    "toolTipMessage": "This alarm cannot be put into access"
                },
                "conditionsToMeetInOrderToDisplay": {
                    "hasPermissions": [
                        "ACCESS"
                    ]
                }
            }
        }
    },
    {
        "label": {
            "sensorName": "2904_COMSEC_(CS)_BMS_Fr_Door",
            "alarmType": "LINE_FAULT_CUT"
        },
        "sensorName": "2904_COMSEC_(CS)_BMS_Fr_Door",
        "alarmType": "LINE_FAULT_CUT",
        "sensorType": "SENTROL2800T",
        "lat": 35.35882195,
        "lon": -77.96413215,
        "geohash": "dq291uuk0",
        "zone": 5,
        "facilityID": "2904",
        "floor": 1,
        "assetNumber": "TBD",
        "lastModified": 1633440297464,
        "notes": "",
        "alarmID": "fd986836-e94f-4bbd-a6dd-b870286d2a90",
        "priority": 4,
        "category": "SECURITY",
        "groupAccess": false,
        "operatorAccess": false,
        "active": false,
        "acknowledgementTimeout": false,
        "facilityName": "4th Communications Sq",
        "stateInputs": {
            "sensorReported": "SECURE",
            "workflowRequested": "SECURE",
            "userRequested": "SECURE"
        },
        "state": "SECURE",
        "alarmTime": -1,
        "additionalAlarmDetails": {},
        "nextValidActions": {
            "access": {
                "displayProperties": {
                    "isDisabled": true,
                    "toolTipMessage": "This alarm cannot be put into access"
                },
                "conditionsToMeetInOrderToDisplay": {
                    "hasPermissions": [
                        "ACCESS"
                    ]
                }
            }
        }
    },
    {
        "label": {
            "sensorName": "2904_COMSEC_(CS)_BMS_Fr_Door",
            "alarmType": "LINE_FAULT_SHORT"
        },
        "sensorName": "2904_COMSEC_(CS)_BMS_Fr_Door",
        "alarmType": "LINE_FAULT_SHORT",
        "sensorType": "SENTROL2800T",
        "lat": 35.35882195,
        "lon": -77.96413215,
        "geohash": "dq291uuk0",
        "zone": 5,
        "facilityID": "2904",
        "floor": 1,
        "assetNumber": "TBD",
        "lastModified": 1633440297464,
        "notes": "",
        "alarmID": "00f4e6a7-9021-4a2b-85f1-45a44f21a106",
        "priority": 4,
        "category": "SECURITY",
        "groupAccess": false,
        "operatorAccess": false,
        "active": false,
        "acknowledgementTimeout": false,
        "facilityName": "4th Communications Sq",
        "stateInputs": {
            "sensorReported": "SECURE",
            "workflowRequested": "SECURE",
            "userRequested": "SECURE"
        },
        "state": "SECURE",
        "alarmTime": -1,
        "additionalAlarmDetails": {},
        "nextValidActions": {
            "access": {
                "displayProperties": {
                    "isDisabled": true,
                    "toolTipMessage": "This alarm cannot be put into access"
                },
                "conditionsToMeetInOrderToDisplay": {
                    "hasPermissions": [
                        "ACCESS"
                    ]
                }
            }
        }
    },
    {
        "label": {
            "sensorName": "3200_BNCC_(CS)_BMS_2",
            "alarmType": "LINE_FAULT_OUT_OF_RANGE"
        },
        "sensorName": "3200_BNCC_(CS)_BMS_2",
        "alarmType": "LINE_FAULT_OUT_OF_RANGE",
        "sensorType": "SENTROL3045",
        "lat": 35.35333512,
        "lon": -77.96834328,
        "geohash": "dq291gchj",
        "zone": 5,
        "facilityID": "3200",
        "floor": 1,
        "assetNumber": "TBD",
        "lastModified": 1633440297464,
        "notes": "",
        "alarmID": "2892e443-e3f2-41a3-96c5-a22249b46710",
        "priority": 4,
        "category": "SECURITY",
        "groupAccess": false,
        "operatorAccess": false,
        "active": false,
        "acknowledgementTimeout": false,
        "facilityName": "Information Systems Flight",
        "stateInputs": {
            "sensorReported": "SECURE",
            "workflowRequested": "SECURE",
            "userRequested": "SECURE"
        },
        "state": "SECURE",
        "alarmTime": -1,
        "additionalAlarmDetails": {},
        "nextValidActions": {
            "access": {
                "displayProperties": {
                    "isDisabled": true,
                    "toolTipMessage": "This alarm cannot be put into access"
                },
                "conditionsToMeetInOrderToDisplay": {
                    "hasPermissions": [
                        "ACCESS"
                    ]
                }
            }
        }
    },

    {
        "label": {
            "sensorName": "3722_Commissary_(DECA)_PIR",
            "alarmType": "LINE_FAULT_CUT"
        },
        "sensorName": "3722_Commissary_(DECA)_PIR",
        "alarmType": "LINE_FAULT_CUT",
        "sensorType": "ARITECHDD300",
        "lat": 35.35827055,
        "lon": -77.95943157,
        "geohash": "dq291uz8v",
        "zone": 5,
        "facilityID": "3722",
        "floor": 1,
        "assetNumber": "SJ-100550",
        "lastModified": 1633440297464,
        "notes": "",
        "alarmID": "19579c03-a46a-490f-914a-167234dc0a74",
        "priority": 4,
        "category": "SECURITY",
        "groupAccess": false,
        "operatorAccess": false,
        "active": false,
        "acknowledgementTimeout": false,
        "facilityName": "Commissary",
        "stateInputs": {
            "sensorReported": "SECURE",
            "workflowRequested": "SECURE",
            "userRequested": "SECURE"
        },
        "state": "SECURE",
        "alarmTime": -1,
        "additionalAlarmDetails": {},
        "nextValidActions": {
            "access": {
                "displayProperties": {
                    "isDisabled": true,
                    "toolTipMessage": "This alarm cannot be put into access"
                },
                "conditionsToMeetInOrderToDisplay": {
                    "hasPermissions": [
                        "ACCESS"
                    ]
                }
            }
        }
    },
    {
        "label": {
            "sensorName": "4743_PDF_(LRS)_BMS_1",
            "alarmType": "LINE_FAULT_OUT_OF_RANGE"
        },
        "sensorName": "4743_PDF_(LRS)_BMS_1",
        "alarmType": "LINE_FAULT_OUT_OF_RANGE",
        "sensorType": "SENTROL2800T",
        "lat": 35.34359418,
        "lon": -77.96086763,
        "geohash": "dq291fnek",
        "zone": 3,
        "facilityID": "4743",
        "floor": 1,
        "assetNumber": "TBD",
        "lastModified": 1633440297464,
        "notes": "",
        "alarmID": "040fd355-6c92-4b2a-8baf-8a02ddb44068",
        "priority": 4,
        "category": "SECURITY",
        "groupAccess": false,
        "operatorAccess": false,
        "active": false,
        "acknowledgementTimeout": false,
        "facilityName": "DEPLOY PROCESS FAC",
        "stateInputs": {
            "sensorReported": "SECURE",
            "workflowRequested": "SECURE",
            "userRequested": "SECURE"
        },
        "state": "SECURE",
        "alarmTime": -1,
        "additionalAlarmDetails": {},
        "nextValidActions": {
            "access": {
                "displayProperties": {
                    "isDisabled": true,
                    "toolTipMessage": "This alarm cannot be put into access"
                },
                "conditionsToMeetInOrderToDisplay": {
                    "hasPermissions": [
                        "ACCESS"
                    ]
                }
            }
        }
    },
    {
        "label": {
            "sensorName": "4743_PDF_(LRS)_BMS_1",
            "alarmType": "INTRUSION_PORTAL_OPEN"
        },
        "sensorName": "4743_PDF_(LRS)_BMS_1",
        "alarmType": "INTRUSION_PORTAL_OPEN",
        "sensorType": "SENTROL2800T",
        "lat": 35.34359418,
        "lon": -77.96086763,
        "geohash": "dq291fnek",
        "zone": 3,
        "facilityID": "4743",
        "floor": 1,
        "assetNumber": "TBD",
        "lastModified": 1633440297464,
        "notes": "",
        "alarmID": "56a33140-df09-4cec-b030-90d7d5532ea4",
        "priority": 4,
        "category": "SECURITY",
        "groupAccess": false,
        "operatorAccess": false,
        "active": false,
        "acknowledgementTimeout": false,
        "facilityName": "DEPLOY PROCESS FAC",
        "stateInputs": {
            "sensorReported": "SECURE",
            "workflowRequested": "SECURE",
            "userRequested": "SECURE"
        },
        "state": "SECURE",
        "alarmTime": -1,
        "additionalAlarmDetails": {},
        "nextValidActions": {
            "access": {
                "displayProperties": {
                    "isDisabled": true,
                    "toolTipMessage": "This alarm cannot be put into access"
                },
                "conditionsToMeetInOrderToDisplay": {
                    "hasPermissions": [
                        "ACCESS"
                    ]
                }
            }
        }
    },

    {
        "label": {
            "sensorName": "4743_PDF_(LRS)_Wireless_Duress_4",
            "alarmType": "LINE_FAULT_OUT_OF_RANGE"
        },
        "sensorName": "4743_PDF_(LRS)_Wireless_Duress_4",
        "alarmType": "LINE_FAULT_OUT_OF_RANGE",
        "sensorType": "INVONICSWIRELESS",
        "lat": 35.34361414,
        "lon": -77.96085484,
        "geohash": "dq291fnek",
        "zone": 3,
        "facilityID": "4743",
        "floor": 1,
        "assetNumber": "TBD",
        "lastModified": 1633440297464,
        "notes": "",
        "alarmID": "069e5e1c-24e0-4e4d-b4c4-e1c3de6df299",
        "priority": 4,
        "category": "SECURITY",
        "groupAccess": false,
        "operatorAccess": false,
        "active": false,
        "acknowledgementTimeout": false,
        "facilityName": "DEPLOY PROCESS FAC",
        "stateInputs": {
            "sensorReported": "SECURE",
            "workflowRequested": "SECURE",
            "userRequested": "SECURE"
        },
        "state": "SECURE",
        "alarmTime": -1,
        "additionalAlarmDetails": {},
        "nextValidActions": {
            "access": {
                "displayProperties": {
                    "isDisabled": true,
                    "toolTipMessage": "This alarm cannot be put into access"
                },
                "conditionsToMeetInOrderToDisplay": {
                    "hasPermissions": [
                        "ACCESS"
                    ]
                }
            }
        }
    },
    {
        "label": {
            "sensorName": "4743_PDF_(LRS)_Wireless_Duress_5",
            "alarmType": "DURESS"
        },
        "sensorName": "4743_PDF_(LRS)_Wireless_Duress_5",
        "alarmType": "DURESS",
        "sensorType": "INVONICSWIRELESS",
        "lat": 35.34355396,
        "lon": -77.96083978,
        "geohash": "dq291fneh",
        "zone": 3,
        "facilityID": "4743",
        "floor": 1,
        "assetNumber": "TBD",
        "lastModified": 1633440297464,
        "notes": "",
        "alarmID": "53318cf5-d678-41f3-902b-c37f4e0686c0",
        "priority": 4,
        "category": "SECURITY",
        "groupAccess": false,
        "operatorAccess": false,
        "active": false,
        "acknowledgementTimeout": false,
        "facilityName": "DEPLOY PROCESS FAC",
        "stateInputs": {
            "sensorReported": "SECURE",
            "workflowRequested": "SECURE",
            "userRequested": "SECURE"
        },
        "state": "SECURE",
        "alarmTime": -1,
        "additionalAlarmDetails": {},
        "nextValidActions": {
            "access": {
                "displayProperties": {
                    "isDisabled": true,
                    "toolTipMessage": "This alarm cannot be put into access"
                },
                "conditionsToMeetInOrderToDisplay": {
                    "hasPermissions": [
                        "ACCESS"
                    ]
                }
            }
        }
    },
    {
        "label": {
            "sensorName": "4743_PDF_(LRS)_Wireless_Duress_6",
            "alarmType": "LINE_FAULT_SHORT"
        },
        "sensorName": "4743_PDF_(LRS)_Wireless_Duress_6",
        "alarmType": "LINE_FAULT_SHORT",
        "sensorType": "INVONICSWIRELESS",
        "lat": 35.34355396,
        "lon": -77.96083978,
        "geohash": "dq291fneh",
        "zone": 3,
        "facilityID": "4743",
        "floor": 1,
        "assetNumber": "TBD",
        "lastModified": 1633440297464,
        "notes": "",
        "alarmID": "935ff915-5c6e-4d45-9bc6-dd971d4fe850",
        "priority": 4,
        "category": "SECURITY",
        "groupAccess": false,
        "operatorAccess": false,
        "active": false,
        "acknowledgementTimeout": false,
        "facilityName": "DEPLOY PROCESS FAC",
        "stateInputs": {
            "sensorReported": "SECURE",
            "workflowRequested": "SECURE",
            "userRequested": "SECURE"
        },
        "state": "SECURE",
        "alarmTime": -1,
        "additionalAlarmDetails": {},
        "nextValidActions": {
            "access": {
                "displayProperties": {
                    "isDisabled": true,
                    "toolTipMessage": "This alarm cannot be put into access"
                },
                "conditionsToMeetInOrderToDisplay": {
                    "hasPermissions": [
                        "ACCESS"
                    ]
                }
            }
        }
    },

    {
        "label": {
            "sensorName": "2102_Slocumb_ECP_(SFS)_Wireless_Duress",
            "alarmType": "LINE_FAULT_CUT"
        },
        "sensorName": "2102_Slocumb_ECP_(SFS)_Wireless_Duress",
        "alarmType": "LINE_FAULT_CUT",
        "sensorType": "INVONICSWIRELESS",
        "lat": 35.34820588,
        "lon": -77.9795033,
        "geohash": "dq291dcn1",
        "zone": 5,
        "facilityID": "2102",
        "floor": 1,
        "assetNumber": "TBD",
        "lastModified": 1633440297464,
        "notes": "",
        "alarmID": "87a39f1f-f53e-49db-a9f0-9410933619fa",
        "priority": 4,
        "category": "SECURITY",
        "groupAccess": false,
        "operatorAccess": false,
        "active": false,
        "acknowledgementTimeout": false,
        "facilityName": "Gate Entry",
        "stateInputs": {
            "sensorReported": "SECURE",
            "workflowRequested": "SECURE",
            "userRequested": "SECURE"
        },
        "state": "SECURE",
        "alarmTime": -1,
        "additionalAlarmDetails": {},
        "nextValidActions": {
            "access": {
                "displayProperties": {
                    "isDisabled": true,
                    "toolTipMessage": "This alarm cannot be put into access"
                },
                "conditionsToMeetInOrderToDisplay": {
                    "hasPermissions": [
                        "ACCESS"
                    ]
                }
            }
        }
    },
    {
        "label": {
            "sensorName": "2102_Slocumb_ECP_(SFS)_Wireless_Duress",
            "alarmType": "LINE_FAULT_SHORT"
        },
        "sensorName": "2102_Slocumb_ECP_(SFS)_Wireless_Duress",
        "alarmType": "LINE_FAULT_SHORT",
        "sensorType": "INVONICSWIRELESS",
        "lat": 35.34820588,
        "lon": -77.9795033,
        "geohash": "dq291dcn1",
        "zone": 5,
        "facilityID": "2102",
        "floor": 1,
        "assetNumber": "TBD",
        "lastModified": 1633440297464,
        "notes": "",
        "alarmID": "5132a9d4-3a74-4415-81ad-3d1ef2783394",
        "priority": 4,
        "category": "SECURITY",
        "groupAccess": false,
        "operatorAccess": false,
        "active": false,
        "acknowledgementTimeout": false,
        "facilityName": "Gate Entry",
        "stateInputs": {
            "sensorReported": "SECURE",
            "workflowRequested": "SECURE",
            "userRequested": "SECURE"
        },
        "state": "SECURE",
        "alarmTime": -1,
        "additionalAlarmDetails": {},
        "nextValidActions": {
            "access": {
                "displayProperties": {
                    "isDisabled": true,
                    "toolTipMessage": "This alarm cannot be put into access"
                },
                "conditionsToMeetInOrderToDisplay": {
                    "hasPermissions": [
                        "ACCESS"
                    ]
                }
            }
        }
    },
    {
        "label": {
            "sensorName": "2102_Slocumb_ECP_(SFS)_Wireless_Duress",
            "alarmType": "LINE_FAULT_OUT_OF_RANGE"
        },
        "sensorName": "2102_Slocumb_ECP_(SFS)_Wireless_Duress",
        "alarmType": "LINE_FAULT_OUT_OF_RANGE",
        "sensorType": "INVONICSWIRELESS",
        "lat": 35.34820588,
        "lon": -77.9795033,
        "geohash": "dq291dcn1",
        "zone": 5,
        "facilityID": "2102",
        "floor": 1,
        "assetNumber": "TBD",
        "lastModified": 1633440297464,
        "notes": "",
        "alarmID": "6521cbc6-dcb0-4800-9c58-84c745ed171d",
        "priority": 4,
        "category": "SECURITY",
        "groupAccess": false,
        "operatorAccess": false,
        "active": false,
        "acknowledgementTimeout": false,
        "facilityName": "Gate Entry",
        "stateInputs": {
            "sensorReported": "SECURE",
            "workflowRequested": "SECURE",
            "userRequested": "SECURE"
        },
        "state": "SECURE",
        "alarmTime": -1,
        "additionalAlarmDetails": {},
        "nextValidActions": {
            "access": {
                "displayProperties": {
                    "isDisabled": true,
                    "toolTipMessage": "This alarm cannot be put into access"
                },
                "conditionsToMeetInOrderToDisplay": {
                    "hasPermissions": [
                        "ACCESS"
                    ]
                }
            }
        }
    },
    {
        "label": {
            "sensorName": "2102_Slocumb_ECP_(SFS)_AC_Fail",
            "alarmType": "AC_FAIL"
        },
        "sensorName": "2102_Slocumb_ECP_(SFS)_AC_Fail",
        "alarmType": "AC_FAIL",
        "sensorType": "ACFAIL",
        "lat": 35.34820212,
        "lon": -77.97948031,
        "geohash": "dq291dcn4",
        "zone": 5,
        "facilityID": "2102",
        "floor": 1,
        "assetNumber": "N/A",
        "lastModified": 1633440297464,
        "notes": "",
        "alarmID": "b6a9f82f-897a-42a8-a37d-8b28699ead4f",
        "priority": 4,
        "category": "SECURITY",
        "groupAccess": false,
        "operatorAccess": false,
        "active": false,
        "acknowledgementTimeout": false,
        "facilityName": "Gate Entry",
        "stateInputs": {
            "sensorReported": "SECURE",
            "workflowRequested": "SECURE",
            "userRequested": "SECURE"
        },
        "state": "SECURE",
        "alarmTime": -1,
        "additionalAlarmDetails": {},
        "nextValidActions": {
            "access": {
                "displayProperties": {
                    "isDisabled": true,
                    "toolTipMessage": "This alarm cannot be put into access"
                },
                "conditionsToMeetInOrderToDisplay": {
                    "hasPermissions": [
                        "ACCESS"
                    ]
                }
            }
        }
    },

    {
        "label": {
            "sensorName": "2205_MUNS_ECP_(MUNS)_AC_Fail",
            "alarmType": "PS_ACFAIL"
        },
        "sensorName": "2205_MUNS_ECP_(MUNS)_AC_Fail",
        "alarmType": "PS_ACFAIL",
        "sensorType": "ACFAIL",
        "lat": 35.34076969,
        "lon": -77.98448285,
        "geohash": "dq2913t6v",
        "zone": 2,
        "facilityID": "2205",
        "floor": 1,
        "assetNumber": "N/A",
        "lastModified": 1633440297464,
        "notes": "",
        "alarmID": "7191a4d0-a7de-47a7-b037-5b07f038885c",
        "priority": 4,
        "category": "SECURITY",
        "groupAccess": false,
        "operatorAccess": false,
        "active": false,
        "acknowledgementTimeout": false,
        "facilityName": "Munitions Flight",
        "stateInputs": {
            "sensorReported": "SECURE",
            "workflowRequested": "SECURE",
            "userRequested": "SECURE"
        },
        "state": "SECURE",
        "alarmTime": -1,
        "additionalAlarmDetails": {},
        "nextValidActions": {
            "access": {
                "displayProperties": {
                    "isDisabled": true,
                    "toolTipMessage": "This alarm cannot be put into access"
                },
                "conditionsToMeetInOrderToDisplay": {
                    "hasPermissions": [
                        "ACCESS"
                    ]
                }
            }
        }
    },
    {
        "label": {
            "sensorName": "2803_Mental_Health_(MDG)_Wired_Duress_1",
            "alarmType": "LINE_FAULT_CUT"
        },
        "sensorName": "2803_Mental_Health_(MDG)_Wired_Duress_1",
        "alarmType": "LINE_FAULT_CUT",
        "sensorType": "UNITEDSECURITYHUB",
        "lat": 35.362315,
        "lon": -77.961237,
        "geohash": "dq291vw27",
        "zone": 5,
        "facilityID": "2803",
        "floor": 2,
        "assetNumber": "TBD",
        "lastModified": 1633440297464,
        "notes": "",
        "alarmID": "d5462d59-4e6c-450a-b9cd-222a335c8063",
        "priority": 4,
        "category": "SECURITY",
        "groupAccess": false,
        "operatorAccess": false,
        "active": false,
        "acknowledgementTimeout": false,
        "facilityName": "4th Medical Group",
        "stateInputs": {
            "sensorReported": "SECURE",
            "workflowRequested": "SECURE",
            "userRequested": "SECURE"
        },
        "state": "SECURE",
        "alarmTime": -1,
        "additionalAlarmDetails": {},
        "nextValidActions": {
            "access": {
                "displayProperties": {
                    "isDisabled": true,
                    "toolTipMessage": "This alarm cannot be put into access"
                },
                "conditionsToMeetInOrderToDisplay": {
                    "hasPermissions": [
                        "ACCESS"
                    ]
                }
            }
        }
    },

    {
        "label": {
            "sensorName": "4513_ITN_(CS)_Low_Batt",
            "alarmType": "PS_LOWBATT"
        },
        "sensorName": "4513_ITN_(CS)_Low_Batt",
        "alarmType": "PS_LOWBATT",
        "sensorType": "LOWBATT",
        "lat": 35.34443468,
        "lon": -77.96621911,
        "geohash": "dq291f68r",
        "zone": 3,
        "facilityID": "4513",
        "floor": 1,
        "assetNumber": "N/A",
        "lastModified": 1633440297464,
        "notes": "",
        "alarmID": "6cd309ea-0bbd-4f1b-9373-0468d5474c9c",
        "priority": 4,
        "category": "SECURITY",
        "groupAccess": false,
        "operatorAccess": false,
        "active": false,
        "acknowledgementTimeout": false,
        "facilityName": "COMM FCLTY",
        "stateInputs": {
            "sensorReported": "SECURE",
            "workflowRequested": "SECURE",
            "userRequested": "SECURE"
        },
        "state": "SECURE",
        "alarmTime": -1,
        "additionalAlarmDetails": {
            "batteryPercentage": 100
        },
        "nextValidActions": {
            "access": {
                "displayProperties": {
                    "isDisabled": true,
                    "toolTipMessage": "This alarm cannot be put into access"
                },
                "conditionsToMeetInOrderToDisplay": {
                    "hasPermissions": [
                        "ACCESS"
                    ]
                }
            }
        }
    },
    {
        "label": {
            "sensorName": "3735_Cr_Union_(NCCFCU)_Entry_BMS",
            "alarmType": "LINE_FAULT"
        },
        "sensorName": "3735_Cr_Union_(NCCFCU)_Entry_BMS",
        "alarmType": "LINE_FAULT",
        "sensorType": "SENTROL2800T",
        "lat": 35.35885738,
        "lon": -77.95979553,
        "geohash": "dq291uzkm",
        "zone": 5,
        "facilityID": "3735",
        "floor": 1,
        "assetNumber": "TBD",
        "lastModified": 1633440297464,
        "notes": "",
        "alarmID": "ca5e47c2-1433-49e5-96fb-8de9c868a7e6",
        "priority": 4,
        "category": "SECURITY",
        "groupAccess": false,
        "operatorAccess": false,
        "active": false,
        "acknowledgementTimeout": false,
        "facilityName": "Base Exchange",
        "stateInputs": {
            "sensorReported": "SECURE",
            "workflowRequested": "SECURE",
            "userRequested": "SECURE"
        },
        "state": "SECURE",
        "alarmTime": -1,
        "additionalAlarmDetails": {},
        "nextValidActions": {
            "access": {
                "displayProperties": {
                    "isDisabled": true,
                    "toolTipMessage": "This alarm cannot be put into access"
                },
                "conditionsToMeetInOrderToDisplay": {
                    "hasPermissions": [
                        "ACCESS"
                    ]
                }
            }
        }
    },
    {
        "label": {
            "sensorName": "3735_Cr_Union_(NCCFCU)_Entry_BMS",
            "alarmType": "LINE_FAULT_CUT"
        },
        "sensorName": "3735_Cr_Union_(NCCFCU)_Entry_BMS",
        "alarmType": "LINE_FAULT_CUT",
        "sensorType": "SENTROL2800T",
        "lat": 35.35885738,
        "lon": -77.95979553,
        "geohash": "dq291uzkm",
        "zone": 5,
        "facilityID": "3735",
        "floor": 1,
        "assetNumber": "TBD",
        "lastModified": 1633440297464,
        "notes": "",
        "alarmID": "5275ad71-864a-497b-819d-f64b5f3efd0b",
        "priority": 4,
        "category": "SECURITY",
        "groupAccess": false,
        "operatorAccess": false,
        "active": false,
        "acknowledgementTimeout": false,
        "facilityName": "Base Exchange",
        "stateInputs": {
            "sensorReported": "SECURE",
            "workflowRequested": "SECURE",
            "userRequested": "SECURE"
        },
        "state": "SECURE",
        "alarmTime": -1,
        "additionalAlarmDetails": {},
        "nextValidActions": {
            "access": {
                "displayProperties": {
                    "isDisabled": true,
                    "toolTipMessage": "This alarm cannot be put into access"
                },
                "conditionsToMeetInOrderToDisplay": {
                    "hasPermissions": [
                        "ACCESS"
                    ]
                }
            }
        }
    },

]

export function AlarmItemCreator() {
    const [inputValue, setInputValue] = useState(globalAlarms);
    const setAlarmState = useSetRecoilState(alarmListState);

    const addAlarm = () => {
        setAlarmState((prevAlarmList) => [
            ...prevAlarmList,
            {
                "label": {
                    "sensorName": "1904A_K9_ARMAG_(SFS)_Keypad",
                    "alarmType": "TAMPER"
                },
                "sensorName": "1904A_K9_ARMAG_(SFS)_Keypad",
                "alarmType": "TAMPER",
                "sensorType": "LINEAR2000E",
                "lat": 35.3580833,
                "lon": -77.9698056,
                "geohash": "dq291u8pg",
                "zone": 5,
                "facilityID": "1904",
                "floor": 1,
                "assetNumber": "TBD",
                "lastModified": 1633440297463,
                "notes": "",
                "alarmID": "f913c7c9-85a9-4ec5-9bf9-f097e91a96bd",
                "priority": 1,
                "category": "SECURITY",
                "groupAccess": false,
                "operatorAccess": false,
                "active": false,
                "acknowledgementTimeout": false,
                "facilityName": "Vet Clinic",
                "stateInputs": {
                    "sensorReported": "SECURE",
                    "workflowRequested": "SECURE",
                    "userRequested": "SECURE"
                },
                "state": "SECURE",
                "alarmTime": -1,
                "additionalAlarmDetails": {},
                "nextValidActions": {
                    "access": {
                        "displayProperties": {
                            "isDisabled": true,
                            "toolTipMessage": "This alarm cannot be put into access"
                        },
                        "conditionsToMeetInOrderToDisplay": {
                            "hasPermissions": [
                                "ACCESS"
                            ]
                        }
                    }
                }
            },
        ]);
        setInputValue('');
    };

    const onChange = ({target: {value}}) => {
        setInputValue(value);
    };

    // Dubious Benchmarks
    // 10,000 is about  12 seconds
    // 5000 events is about 4 seconds to add to the queue
    // 1000 instant
    const letsGo = ()=>{
        for(let i=0; i<10000;++i){
            console.log(i);
            addAlarm();
        }
    }

    const removeAllTheAlarms = ()=>{
        setAlarmState({});  // this crashes but just refresh
    }

    return (
        <div>
            <input type="text" value={inputValue} onChange={onChange} />
            <button onClick={letsGo}>ADD ALL THE ALARMS</button>
            <button onClick={removeAllTheAlarms}>Clear EVERYTHING</button>
        </div>
    );
}
