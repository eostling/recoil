import React  from 'react';
import { Box } from '@chakra-ui/react';
import IconButtonWithTooltip from './IconButtonWithTooltip';

const AlarmCardStateButton = ({state}) => {
    return (
            <Box>
                <IconButtonWithTooltip
                    placement='left'
                    color={`states.${state}`}
                    size='m'
                    p={1}
                    colorScheme={state}
                    ariaLabel='alarm-card-state-button'
                />
            </Box>
        )
};

export default AlarmCardStateButton;
