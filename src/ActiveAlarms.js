import React from 'react';
import {
    useRecoilState,
    useRecoilValue,
} from 'recoil';
import {charState} from "./recoil/atoms/charState";
import {charCountSelector} from "./recoil/selectors/charCountSelector";

const ActiveAlarms = ({activeAlarms}) => {

    const [alarm, setAlarm] = useRecoilState(charState);

    const onChange = (event) => {
        setAlarm(event.target.value);
    };

    const count = useRecoilValue(charCountSelector);

    return (
        <>
            <>Alarm Count: {count}</>;
            <input type="text" value={alarm} onChange={onChange} />
             <br />
             Echo Derived State: {alarm}
            <ul>
                <li>{activeAlarms.sensorName}</li>
            </ul>
        </>
    );
};

export default ActiveAlarms;
