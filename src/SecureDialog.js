import React, {  useRef, useState } from 'react';
import {
    Button,
    Divider,
    Flex,
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalFooter,
    ModalHeader,
    ModalOverlay,
    Select,
    Stack,
    Textarea,
} from '@chakra-ui/react';

import axios from 'axios';


const SecureDialog = ({
    isOpen,
    onClose,
    alarmList,
}) => {
    const [assessment, setAssessment] = useState('Valid');
    const [remark, setRemark] = useState('');
    const initialFocus = useRef(null);

    const updateMultiAlarmStateSecureDialog = async (
        alarms,
        userRemark,
        updatedAssessment,
        newState
    ) => {
        let urlState = newState.toLowerCase();
        if (urlState === 'acknowledged') {
            urlState = 'acknowledge';
        }

        const updateURL = `/api/alarms/actions/${urlState}/`;

        try {
            const { data } = await axios.put(updateURL, {
                alarms,
                userRemark,
                updatedAssessment,
            });
            return data;
        } catch (err) {
            return err;
        }
    };

    const handleAssessment = (e) => {
        const inputValue = e.target.value;
        setAssessment(inputValue);
    };

    const handleRemark = (e) => {
        const inputValue = e.target.value;
        setRemark(inputValue);
    };

    const onSubmit = async () => {
        try {
            const result = await updateMultiAlarmStateSecureDialog(
                alarmList,
                remark,
                assessment,
                'SECURE',
            );
            if (result) {
                const updatedAlarms = result.filter((resp) => {
                    return resp.statusCode === 200;
                });
                const alarms = updatedAlarms.length === 1 ? 'Alarm' : 'Alarms';
                return console.log(1);
            }
            return console.log(1);
        } catch (err) {
            return console.log(1);
        } finally {
            setAssessment('Valid');
            setRemark('');
            onClose();
        }
    };

    const getLabel = (alarm) => {
        const { label } = alarm;
        return label;
    };

    return (
        <>
            <Modal
                initialFocusRef={initialFocus}
                isOpen={isOpen}
                onClose={onClose}
                scrollBehavior='inside'
            >
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader color='white'>
                        {`Secure ${alarmList.length > 1 ? 'Alarms' : 'Alarm'}`}
                    </ModalHeader>
                    <ModalCloseButton color='white' />
                    <ModalBody>
                        <Stack spacing={2}>
                            {alarmList.map((alarm) => {
                                return (
                                    <Flex key={alarm.label} color='white'>
                                        {getLabel(alarm)}
                                    </Flex>
                                );
                            })}
                        </Stack>
                        <Divider borderColor='gray.900' />
                        <Stack spacing={2}>
                            <Select
                                onChange={handleAssessment}
                                value={assessment}
                                bg='gray.700'
                                ref={initialFocus}
                            >
                                <option value='Valid'>Valid</option>
                                <option value='Nuisance'>Nuisance</option>
                                <option value='False Positive'>False Positive</option>
                            </Select>
                            <Textarea
                                value={remark}
                                onChange={handleRemark}
                                placeholder='Optional remark...'
                                size='sm'
                                color='white'
                            />
                        </Stack>
                    </ModalBody>
                    <ModalFooter>
                        <Button variant='outline' mr={3} onClick={onClose}>
                            Cancel
                        </Button>
                        <Button type='submit' colorScheme='green' onClick={onSubmit}>
                            Secure
                        </Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </>
    );
};

export default SecureDialog;
