import React  from 'react';
import { Box, Button, Flex, Stack } from '@chakra-ui/react';

import AlarmCardStateButton from './AlarmCardStateButton';

const AlarmCard = ({ alarm }) => {
    const { label, state } =
        alarm;
    return (
        <Flex
            bg='gray.700'
            w='100%'
            flexDirection='column'
            borderRadius={3}
            overflow='hidden'
        >
            <Flex
                p={2}
                flexDirection='row'
                alignItems='center'
                justify='space-between'
            >
                <Stack spacing={2} direction='row'>
                        <Button
                            maxWidth='430px'
                            textAlign='left'
                            overflow='hidden'
                            textOverflow='ellipsis'
                            display='block'
                            whiteSpace='nowrap'
                            variant='link'
                            fontSize='.8em'
                            colorScheme={`states.${state}`}
                        >
                            {`${label}`}
                        </Button>
                </Stack>
                <Box>
                    <Stack direction='row' spacing={2}>
                        <AlarmCardStateButton
                            state='ACK_UNKNOWN'
                        />
                    </Stack>
                </Box>
            </Flex>
        </Flex>
    );
};

export default AlarmCard;
