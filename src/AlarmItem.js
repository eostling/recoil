import {useRecoilState} from 'recoil';
import {alarmListState} from "./recoil/atoms/alarmListState";


export function AlarmItem({item}) {
    const [alarmList, setAlarmList] = useRecoilState(alarmListState);
    const index = alarmList.findIndex((listItem) => listItem === item);

    const editAlarmText = ({target: {value}}) => {
        const newList = replaceItemAtIndex(alarmList, index, {
            ...item,
            text: value,
        });

        setAlarmList(newList);
    };

    const toggleItemCompletion = () => {
        const newList = replaceItemAtIndex(alarmList, index, {
            ...item,
            isComplete: !item.isComplete,
        });

        setAlarmList(newList);
    };

    const deleteItem = () => {
        const newList = removeItemAtIndex(alarmList, index);

        setAlarmList(newList);
    };

    return (
        <div>
            <input type="text" value={item.text} onChange={editAlarmText} />
            <input
                type="checkbox"
                checked={item.isComplete}
                onChange={toggleItemCompletion}
            />
            <button onClick={deleteItem}>X</button>
        </div>
    );
}

function replaceItemAtIndex(arr, index, newValue) {
    return [...arr.slice(0, index), newValue, ...arr.slice(index + 1)];
}

function removeItemAtIndex(arr, index) {
    return [...arr.slice(0, index), ...arr.slice(index + 1)];
}
