import {
    useRecoilValue,
} from 'recoil';
import { alarmListState } from "./recoil/atoms/alarmListState";
import { AlarmItemCreator } from "./AlarmItemCreator";
import { AlarmItem } from "./AlarmItem";

export function AlarmList() {
    const alarmList = useRecoilValue(alarmListState);

    return (
        <>
            <AlarmItemCreator />
            {alarmList.map((alarmItem) => (
                <AlarmItem key={alarmItem.id} item={alarmItem} />
            ))}
        </>
    );
}
